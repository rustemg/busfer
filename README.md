###SDK
* [*Bitnami RubyStack v2.0.0*](https://bitnami.com/stack/ruby/installer/)
* [*Rails v4.2.5.1*](http://rubyonrails.org/)  
`> gem install rails -v 4.2.5.1`
* [*ImageMagick v6.9.3-8*](http://www.imagemagick.org/script/binary-releases.php)