Rails.application.routes.draw do
  resources :photos
  resources :buses
  root 'buses#index'
end
