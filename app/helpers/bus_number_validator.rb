class BusNumberValidator < ActiveModel::Validator
  def validate(bus)
    error = false

    l = bus.number.length
    error = l < 8 or l > 9        

    if not error
        valid_letters = Bus.valid_letters_in_number
        valid_digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

        s = bus.number
        digits = s[1..3] + s[6..-1]
        letters = s[0] + s[4..5]

        error = check(digits, valid_digits) or check(letters, valid_letters)
    end

    if error
        bus.errors[:base] << "Номер должен быть в формате А000АА00 или А000АА000"
    end        
  end

  private

  def check(s, arr)
    error = false
    s.each_char do |ch|
        if not arr.include?(ch)
            error = true;
        end
    end

    return error
  end

end