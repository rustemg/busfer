class Bus < ActiveRecord::Base

    has_many :photos, dependent: :destroy

    #accepts_nested_attributes_for :photos

    validates :name, presence: true
    validates :number, uniqueness: { case_sensitive: false }
    validates :pts, presence: true
    validates :pts_date, presence: true

     validates_with BusNumberValidator, fields: [:number]

    def self.valid_letters_in_number
        ['А', 'В', 'Е', 'К', 'М', 'Н', 'О', 'Р', 'С', 'Т', 'У', 'Х']
    end

    def description
        return "##{id} #{name} #{number}"
    end
end
