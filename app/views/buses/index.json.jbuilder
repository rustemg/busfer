json.array!(@buses) do |bus|
  json.extract! bus, :id, :name, :number, :pts, :pts_date
  json.url bus_url(bus, format: :json)
end
