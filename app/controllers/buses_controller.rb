require 'unicode'

class BusesController < ApplicationController
  before_action :set_bus, only: [:show, :edit, :update, :destroy]

  # GET /buses
  # GET /buses.json
  def index
    @buses = Bus.all
  end

  # GET /buses/1
  # GET /buses/1.json
  def show
  end

  # GET /buses/new
  def new
    @bus = Bus.new
    if Rails.env = "development"
      @bus.name = "Автобус_" + (65 + rand(26)).chr + rand(100).to_s
      @bus.pts = "ПТС_" + (65 + rand(26)).chr + rand(100).to_s
      valid_letters = Bus.valid_letters_in_number
      @bus.number = valid_letters[rand(12)] + rand(10).to_s + rand(10).to_s + rand(10).to_s + 
        (0..1).map{ valid_letters[rand(12)] }.join + rand(10).to_s + rand(10).to_s
    end
  end

  # GET /buses/1/edit
  def edit
  end

  # POST /buses
  # POST /buses.json
  def create
    @bus = Bus.new(bus_params)

    respond_to do |format|
      if @bus.save        
        save_photos
        format.html { redirect_to buses_path, notice: "Создано: #{@bus.description}" }
        format.json { render :show, status: :created, location: @bus }
      else
        format.html { render :new }
        format.json { render json: @bus.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /buses/1
  # PATCH/PUT /buses/1.json
  def update
    respond_to do |format|
      if @bus.update(bus_params)
        save_photos
        format.html { redirect_to buses_path, notice: "Обновлено: #{@bus.description}" }
        format.json { render :show, status: :ok, location: @bus }
      else
        format.html { render :edit }
        format.json { render json: @bus.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /buses/1
  # DELETE /buses/1.json
  def destroy
    @bus.destroy
    respond_to do |format|
      format.html { redirect_to buses_url, notice: "Удалено: #{@bus.description}" }
      format.json { head :no_content }
    end
  end

  private
def save_photos
  if not params[:photos].nil?
    params[:photos]['image'].each do |i|
      @bus.photos.create!(:image => i)
    end
  end
end

    # Use callbacks to share common setup or constraints between actions.
    def set_bus
      @bus = Bus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bus_params
      h = params.require(:bus).permit(:name, :number, :pts, :pts_date)
      h[:number] = Unicode::upcase(h[:number])
      return h
    end
end
